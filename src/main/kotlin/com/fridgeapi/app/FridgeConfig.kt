package com.fridgeapi.app

import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDate
import java.time.Month
import java.util.*

@Configuration
class FridgeConfig {
    @Bean
    fun commandLineRunner(fridgeRepository: FridgeRepository): CommandLineRunner? {
        return CommandLineRunner {
            val beef = Fridge(1L,"Beef", "Meet", 1, LocalDate.of(2022, Month.JUNE, 8))
            val chickenEgg = Fridge(2L,"Chicken egg", "Eggs", 1, LocalDate.of(2022, Month.JULY, 12))
            fridgeRepository.saveAll(Collections.unmodifiableList(Arrays.asList(beef,chickenEgg)))
        }
    }
}