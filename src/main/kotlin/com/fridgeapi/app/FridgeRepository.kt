package com.fridgeapi.app

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface FridgeRepository : JpaRepository<Fridge, Long>
{
    @Query("SELECT s FROM Fridge s where s.name = ?1")
    fun findFoodByIs(foodId: Long): Optional<Fridge?>?
}