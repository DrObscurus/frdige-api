package com.fridgeapi.app

import org.springframework.http.HttpStatus

class FridgeNotFoundException(val statusCode: HttpStatus, val reason: String) : Exception()