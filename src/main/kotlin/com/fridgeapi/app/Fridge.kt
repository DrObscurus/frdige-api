package com.fridgeapi.app

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "fridge")
data class Fridge(
    @Id
    @SequenceGenerator(name = "fridge_sequence", sequenceName = "fridge_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fridge_sequence")

    var id: Long?,
    @Column(name = "food_name", unique = true, nullable = false)
    val name: String,
    @Column(name = "food_type", nullable = false)
    val type: String,
    @Column(name = "food_amount", nullable = false)
    val amount: Int,
    @Column(name = "food_expiration_date", nullable = true)
    val expirationDate: LocalDate?)
{
    override fun toString(): String {
        return "Food{" +
                "id=" + id +
                ", food_name='" + name + '\'' +
                ", food_type='" + type + '\'' +
                ", food_amount=" + amount +
                ", food_expiration_date=" + expirationDate +
                '}'
    }
}