package com.fridgeapi.app

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class FridgeService(private val fridgeRepository: FridgeRepository) {
    fun getAllFood(): List<Fridge> = fridgeRepository.findAll()

    fun getFoodById(foodId: Long): Fridge = fridgeRepository.findById(foodId)
        .orElseThrow { FridgeNotFoundException(HttpStatus.NOT_FOUND, "No matching food was found") }

    fun addNewFood(food: Fridge): Fridge = fridgeRepository.save(food)

    fun updateFoodById(foodId: Long, fridge: Fridge): Fridge {
        return if (fridgeRepository.existsById(foodId)) {
            fridgeRepository.save(
                Fridge(
                    id = fridge.id,
                    name = fridge.name,
                    type = fridge.type,
                    amount = fridge.amount,
                    expirationDate = fridge.expirationDate
                )
            )
        } else throw FridgeNotFoundException(HttpStatus.NOT_FOUND, "No matching food was found")
    }

    fun deleteFoodById(foodId: Long) {
        return if (fridgeRepository.existsById(foodId)) {
            fridgeRepository.deleteById(foodId)
        } else throw FridgeNotFoundException(HttpStatus.NOT_FOUND, "No matching food was found")
    }
}