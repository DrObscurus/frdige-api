package com.fridgeapi.app

import org.springframework.web.bind.annotation.*

@RestController
class FridgeController(private val fridgeService: FridgeService) {
    @GetMapping("/fridge")
    fun getAllFood(): List<Fridge> = fridgeService.getAllFood()

    @GetMapping("/fridge/{id}")
    fun getFoodById(@PathVariable("id") foodId: Long): Fridge =
        fridgeService.getFoodById(foodId)

    @PostMapping("/fridge")
    fun addFood(@RequestBody payload: Fridge): Fridge = fridgeService.addNewFood(payload)

    @PutMapping("/fridge/{id}")
    fun updateFoodById(@PathVariable("id") foodId: Long, @RequestBody payload: Fridge): Fridge =
        fridgeService.updateFoodById(foodId, payload) // ISSUE NEED TO CHECK

    @DeleteMapping("/fridge/{id}")
    fun deleteFoodById(@PathVariable("id") foodId: Long): Unit =
        fridgeService.deleteFoodById(foodId)
}