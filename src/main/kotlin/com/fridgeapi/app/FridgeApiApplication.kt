package com.fridgeapi.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FridgeApiApplication

fun main(args: Array<String>) {
    runApplication<FridgeApiApplication>(*args)
}
