CREATE TABLE IF NOT EXISTS food (
    id  INT PRIMARY KEY,
    food_name    VARCHAR(100) NOT NULL,
    food_type    VARCHAR(100) NOT NULL,
    food_amount    INT NOT NULL,
    food_expiration_date    VARCHAR(100) NOT NULL
);